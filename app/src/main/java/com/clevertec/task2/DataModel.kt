package com.clevertec.task2


data class DataModel(
    val title: String?,
    val description: String?,
)
