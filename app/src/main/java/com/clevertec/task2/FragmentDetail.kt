package com.clevertec.task2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


import androidx.fragment.app.Fragment
import com.clevertec.task2.databinding.FragmentDetailBinding


class FragmentDetail : Fragment() {

private lateinit var binding:FragmentDetailBinding
    private var title: String? = null
    private var description: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            title = it.getString(TITLE)
            description = it.getString(DESCRIPTION)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDetailBinding.inflate(inflater)
        val view = binding.root
        workWithToolBar()
        val title = binding.titleDetail
        val description = binding.descriptionDetail
        title.text = this.title
        description.text = this.description
        return view


    }

    private fun workWithToolBar() {
        (activity as MainActivity).supportActionBar?.show()
        (activity as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as MainActivity).supportActionBar?.setHomeButtonEnabled(true)
        (activity as MainActivity).supportActionBar?.title = "DetailFragment"
    }

    override fun onResume() {
        super.onResume()
        binding.floatingActionButton.setOnClickListener {
            activity?.finish()
        }
    }
    override fun onDestroyView() {
        super.onDestroyView()
        (activity as MainActivity).supportActionBar?.hide()
    }

    companion object {

        private const val TITLE = "title"
        private const val DESCRIPTION = "description"

        @JvmStatic
        fun newInstance(title: String, description: String) =
            FragmentDetail().apply {
                arguments = Bundle().apply {
                    putString(TITLE, title)
                    putString(DESCRIPTION, description)
                }
            }
    }
}


