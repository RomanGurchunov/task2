package com.clevertec.task2

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.clevertec.task2.databinding.ActivityItemBinding

class Adapter(var list: List<DataModel>, private val clickListener: ClickListener) :
    RecyclerView.Adapter<Adapter.MyHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        val inflater = LayoutInflater.from(parent.context)
        val itemBinding = ActivityItemBinding.inflate(inflater, parent, false)
        return MyHolder(itemBinding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        holder.bind(list[position])
        holder.itemView.setOnClickListener {
            clickListener.onItemClick(list.get(position))
        }
    }

    inner class MyHolder(private val activityItemBinding: ActivityItemBinding) :
        RecyclerView.ViewHolder(activityItemBinding.root) {

        fun bind(dataModel: DataModel) {
            activityItemBinding.title.text = dataModel.title
            activityItemBinding.description.text = dataModel.description
        }
    }

    interface ClickListener {
        fun onItemClick(dateModel: DataModel)
    }
}
