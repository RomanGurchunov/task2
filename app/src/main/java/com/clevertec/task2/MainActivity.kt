package com.clevertec.task2

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.clevertec.task2.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val fragmentList = FragmentList.newInstance()

        supportFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainerView, fragmentList, "fragment_list")
            .commit()


    }
}
