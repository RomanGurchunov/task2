package com.clevertec.task2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.clevertec.task2.databinding.FragmentListBinding


class FragmentList : Fragment(), Adapter.ClickListener {

    private lateinit var adapter: Adapter
    private var list: ArrayList<DataModel> = ArrayList()
    private lateinit var binding: FragmentListBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentListBinding.inflate(inflater)
        (activity as MainActivity).supportActionBar?.hide()
        val view = binding.root
        buildDisplayData()
        initRecyclerView(view)
        return view
    }

    private fun initRecyclerView(view: View) {
        val recyclerView = view.findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(activity)

        adapter = Adapter(list, this)
        recyclerView.adapter = adapter
    }

    private fun buildDisplayData() {
        var i = 1
        while (i <= 1000) {
            list.add(DataModel("Title$i", "Description$i"))
            i++
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            FragmentList().apply { arguments = Bundle().apply { } }
    }

    override fun onItemClick(dateModel: DataModel) {
        val fragment: Fragment =
            FragmentDetail.newInstance(dateModel.title!!, dateModel.description!!)
        activity?.supportFragmentManager!!.beginTransaction()
            .replace(R.id.fragmentContainerView, fragment, "fragment_list")
            .addToBackStack(null)
            .commit()
    }


}